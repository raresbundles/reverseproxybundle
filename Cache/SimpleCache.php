<?php

namespace Rares\ReverseProxyBundle\Cache;

use Rares\ReverseProxyBundle\Store\SimpleCacheStore;
use Psr\SimpleCache\CacheInterface;
use Symfony\Bundle\FrameworkBundle\HttpCache\HttpCache;
use Symfony\Component\HttpKernel\KernelInterface;

class SimpleCache extends HttpCache
{
    private $store;

    public function __construct(KernelInterface $kernel, CacheInterface $adapter)
    {
        $this->store = new SimpleCacheStore($adapter);

        parent::__construct($kernel, null);
    }

    protected function createStore()
    {
        return $this->store;
    }
}