<?php

namespace Rares\ReverseProxyBundle\Store;

use Psr\SimpleCache\CacheInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpCache\Store;

class SimpleCacheStore extends Store
{
    private $adapter;

    private $keyCache;
    private $locks;

    public function __construct(CacheInterface $adapter)
    {
        $this->adapter = $adapter;

        $this->keyCache = new \SplObjectStorage();
        $this->locks = [];
    }

    /**
     * {@inheritdoc}
     */
    public function cleanup()
    {
        $this->locks = [];
    }

    /**
     * {@inheritdoc}
     */
    public function lock(Request $request)
    {
        $key = $this->getCacheKey($request);

        $this->locks[$key] = true;

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function unlock(Request $request)
    {
        $key = $this->getCacheKey($request);

        if (isset($this->locks[$key])) {
            unset($this->locks[$key]);

            return true;
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function isLocked(Request $request)
    {
        $key = $this->getCacheKey($request);

        if (isset($this->locks[$key])) {
            return true;
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function lookup(Request $request)
    {
        $key = $this->getCacheKey($request);

        if (!$entries = $this->getMetadata($key)) {
            return;
        }

        // find a cached entry that matches the request.
        $match = null;
        foreach ($entries as $entry) {
            if ($this->requestsMatch(isset($entry[1]['vary'][0]) ? implode(', ', $entry[1]['vary']) : '', $request->headers->all(), $entry[0])) {
                $match = $entry;

                break;
            }
        }

        if (null === $match) {
            return;
        }

        list($req, $headers) = $match;
        if ($body = $this->load($match[1]['x-content-digest'][0])) {
            return $this->restoreResponse($headers, $body);
        }

        $this->adapter->delete($headers['x-content-digest'][0]);

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function write(Request $request, Response $response)
    {
        $key = $this->getCacheKey($request);
        $storedEnv = $request->headers->all();

        // write the response body to the entity store if this is the original response
        if (!$response->headers->has('X-Content-Digest')) {
            $digest = $this->generateContentDigest($response);

            $this->adapter->set($digest, $response->getContent());

            $response->headers->set('X-Content-Digest', $digest);

            if (!$response->headers->has('Transfer-Encoding')) {
                $response->headers->set('Content-Length', strlen($response->getContent()));
            }
        }

        // read existing cache entries, remove non-varying, and add this one to the list
        $entries = [];
        $vary = $response->headers->get('vary');
        foreach ($this->getMetadata($key) as $entry) {
            if (!isset($entry[1]['vary'][0])) {
                $entry[1]['vary'] = array('');
            }

            if ($vary != $entry[1]['vary'][0] || !$this->requestsMatch($vary, $entry[0], $storedEnv)) {
                $entries[] = $entry;
            }
        }

        $headers = $this->persistResponse($response);
        unset($headers['age']);

        array_unshift($entries, array($storedEnv, $headers));

        $this->adapter->set($key, $entries);

        return $key;
    }

    /**
     * {@inheritdoc}
     */
    public function invalidate(Request $request)
    {
        $modified = false;
        $key = $this->getCacheKey($request);

        $entries = [];
        foreach ($this->getMetadata($key) as $entry) {
            $response = $this->restoreResponse($entry[1]);
            if ($response->isFresh()) {
                $response->expire();
                $modified = true;
                $entries[] = [$entry[0], $this->persistResponse($response)];
            } else {
                $entries[] = $entry;
            }
        }

        if ($modified) {
            $this->adapter->set($key, $entries);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function purge($url)
    {
        $http = preg_replace('#^https:#', 'http:', $url);
        $https = preg_replace('#^http:#', 'https:', $url);

        $purgedHttp = $this->doPurge($http);
        $purgedHttps = $this->doPurge($https);

        return $purgedHttp || $purgedHttps;
    }

    private function getCacheKey(Request $request)
    {
        if (isset($this->keyCache[$request])) {
            return $this->keyCache[$request];
        }

        return $this->keyCache[$request] = $this->generateCacheKey($request);
    }

    private function getMetadata($key)
    {
        if (!$entries = $this->load($key)) {
            return [];
        }

        return $entries;
    }

    private function load($key)
    {
        return $this->adapter->has($key) ? $this->adapter->get($key) : false;
    }

    private function requestsMatch($vary, $env1, $env2)
    {
        if (empty($vary)) {
            return true;
        }

        foreach (preg_split('/[\s,]+/', $vary) as $header) {
            $key = str_replace('_', '-', strtolower($header));
            $v1 = isset($env1[$key]) ? $env1[$key] : null;
            $v2 = isset($env2[$key]) ? $env2[$key] : null;
            if ($v1 !== $v2) {
                return false;
            }
        }

        return true;
    }

    private function restoreResponse($headers, $body = null)
    {
        $status = $headers['X-Status'][0];
        unset($headers['X-Status']);

        return new Response($body, $status, $headers);
    }

    private function persistResponse(Response $response)
    {
        $headers = $response->headers->all();
        $headers['X-Status'] = [$response->getStatusCode()];

        return $headers;
    }

    private function doPurge($url)
    {
        $key = $this->getCacheKey(Request::create($url));
        if (isset($this->locks[$key])) {
            unset($this->locks[$key]);
        }

        if ($this->adapter->has($key) && $this->adapter->delete($key)) {
            return true;
        }

        return false;
    }
}