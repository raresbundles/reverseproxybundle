# Overview
A Symfony 3 bundle providing compatibility between the Cache Component and the Symfony Reverse Proxy.

# Installation
Require this bundle in your project:

```
composer require "rares/reverse-proxy-bundle"
```

# Usage

Simple use the supplied SimpleCache kernel class instead of the default AppCache class in production.

This class also takes a second argument which is a CacheInterface from the PSR-16 spec. 

```php
$kernel = new \Rares\ReverseProxyBundle\Cache\SimpleCache($kernel, new \Symfony\Component\Cache\Simple\FilesystemCache());
```

You can use any CacheInterface object here.This allows the cache to be stored on the filesystem, in Redis etc.

You can also use the SimpleCacheStore class directly if you have a custom HttpCache kernel, like when using this with the BigPipeBundle.
